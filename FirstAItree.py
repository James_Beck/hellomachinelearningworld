from sklearn import tree

features = [[200, 1, 80], [205, 1, 81], [150, 0, 40], [300, 2, 45]]
labels = [0, 0, 1, 2]

clf = tree.DecisionTreeClassifier()
clf = clf.fit(features, labels)
print(clf.predict([[280,0,40]]))